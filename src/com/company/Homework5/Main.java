package com.company.Homework5;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random(101);
        Human motherJane = new Human("Jane", "Corleone", 1897);
        Human fatherVito = new Human("Vito", "Corleone", 1891);

        Family familyCorleone = new Family(motherJane, fatherVito);

        Human michael = new Human("Michael", "Corleone", 1977, 90,
                new String[][]{{"Sunday", "do home work"}, {"Monday", "go to courses; watch a film"}, {"Tuesday", "do workout"},
                        {"Wednesday", "read e-mails"}, {"Thursday", "do shopping"}, {"Friday", "do household"}, {"Saturday ", "visit grandparents"}});

        michael.setFamily(familyCorleone);
        familyCorleone.addChild(michael);                   // checking addChild(Human human)

        Pet michaelsPet = new Pet("dog", "Rock", 5, random.nextInt(), new String[]{"eat, drink, sleep"});
        familyCorleone.setPet(michaelsPet);
        michael.greetPet();
        michael.describePet();
        michaelsPet.respond();
        michaelsPet.eat();
        michaelsPet.foul();
        michael.feedPet(false);

        System.out.println(michaelsPet.toString()); // toString() of Pet class

        System.out.println(michael.toString());

        Human testch1 = new Human();
        Human testch2 = new Human();
        Human testch3 = new Human();                       // creating three test child objects
        familyCorleone.addChild(testch1);
        familyCorleone.addChild(testch2);
        familyCorleone.addChild(testch3);                  // checking addChild(Human human)
        familyCorleone.countFamily();                      // checking countFamily()
        System.out.println();
        familyCorleone.deleteChild(2);               // checking deleteChild(int index)
        familyCorleone.countFamily();
        System.out.println();
        familyCorleone.deleteChild(testch3);               // checking deleteChild(Human human)
        familyCorleone.countFamily();
    }
}




