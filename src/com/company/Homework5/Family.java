package com.company.Homework5;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Pet pet;
    private Human[] childrenList;

    public Family(Human mother, Human father) {
        setFather(father);
        setMother(mother);
        this.childrenList = new Human[]{};
    }

    public Human getMother() {

        return mother;
    }

    public void setMother(Human mother) {

        this.mother = mother;
    }

    public Human getFather() {

        return father;
    }

    public void setFather(Human father) {

        this.father = father;
    }

    public Pet getPet() {

        return pet;
    }

    public void setPet(Pet pet) {

        this.pet = pet;
    }

    public void addChild(Human human) {
        Human[] childArray = new Human[childrenList.length + 1];
        for (int i = 0; i < childrenList.length; i++) {
            childArray[i] = childrenList[i];
        }
        childArray[childrenList.length] = human;
        this.childrenList = childArray;
    }

    public boolean deleteChild(int index) {
        try {
            boolean deleted = false;
            Human[] childArray = new Human[this.childrenList.length - 1];
            int i = 0;
            for (int j = 0; j < childrenList.length; j++) {
                if (j != index) {
                    childArray[i++] = childrenList[j];
                }
            }
            this.childrenList = childArray;
            if (index < childArray.length - 1) {
                deleted = true;
            }
            return deleted;
        } catch (Exception exception) {
            System.out.println("You write wrong index");
        }
        return false;
    }

    public boolean deleteChild(Human human) {
        boolean deleted = false;
        Human[] childArray = new Human[this.childrenList.length - 1];
        int index = 0;
        for (Human child : this.childrenList) {
            if (!child.equals(human)) {
                childArray[index++] = child;
            }
        }
        this.childrenList = childArray;
        if (index < childArray.length - 1) {
            deleted = true;
        }
        return deleted;
    }

    public int countFamily() {
        int count = 2;
        for (Human human : childrenList) {
            if (human != null) {
                count++;
            }
        }
        System.out.println("Family members are: " + count);
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;
        Family family = (Family) o;
        return Objects.equals(getMother(), family.getMother()) && Objects.equals(getFather(), family.getFather()) && Objects.equals(getPet(), family.getPet()) && Arrays.equals(childrenList, family.childrenList);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getMother(), getFather(), getPet());
        result = 31 * result + Arrays.hashCode(childrenList);
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", pet=" + pet +
                ", childrenList=" + Arrays.toString(childrenList) +
                '}';
    }
}
